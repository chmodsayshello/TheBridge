--20221004_2c

local S = minetest.get_translator("the_bridge")

--team_id_red = 1
--team_id_blue = 2



local modpath = minetest.get_modpath("the_bridge")

minetest.register_privilege("the_bridge_admin", S("Is required in order to manage the bride games!"))

function get_team_id_by_name(modref,p_name)
  for ID, name in pairs(modref.teams) do
    if name == p_name then
      return ID
    end
  end
  return nil
end

arena_lib.register_minigame("the_bridge", {
    name = "The Bridge",
    icon = "icon.png",
    teams = { S("red"), S("blue") },
    teams_color_overlay = { "crimson", "blue"},  
    celebration_time = 3,
    load_time = 6,
    prefix = "[TB] ",
    queue_waiting_time = 20,
    show_minimap = true,
    join_while_in_progress=false,
    properties = {
      the_bridge_area_pos_1 = {x = 0, y = 0, z = 0},
      the_bridge_area_pos_2 = {x = 0, y = 0, z = 0},
      mapdata = nil,
      huds = {},
      team_id_red=nil,
      team_id_blue=nil
    },
    temp_properties = {
      finished = false,
      scoring = false,
      setup = false,
    },
  
    in_game_physics = {
      speed = player_speed,
      jump = player_jump,
      sneak=true,--allow sneaking (again)
    },
  
    disabled_damage_types = {},
    hotbar = {
      slots = 8,
    },
  
    team_properties = {
      goals = 0
    },

    --[[player_properties = {
      scoreboard_red = nil,
      scoreboard_blue = nil,
    },--]]
  
})

if not minetest.get_modpath("lib_chatcmdbuilder") then
  dofile(minetest.get_modpath("the_bridge") .. "/chatcmdbuilder.lua")
end

dofile(minetest.get_modpath("the_bridge") .. "/setup.lua")
dofile(minetest.get_modpath("the_bridge") .. "/hud.lua")
dofile(minetest.get_modpath("the_bridge") .. "/the_bridge.lua")



ChatCmdBuilder.new("the_bridge", function(cmd)

    -- create arena
    cmd:sub("create :arena", function(name, arena_name)
        arena_lib.create_arena(name, "the_bridge", arena_name)
    end)
  
    cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
        arena_lib.create_arena(name, "the_bridge", arena_name, min_players, max_players)
    end)
  
    -- remove arena
    cmd:sub("remove :arena", function(name, arena_name)
        arena_lib.remove_arena(name, "the_bridge", arena_name)
    end)
  
    -- list of the arenas
    cmd:sub("list", function(name)
        arena_lib.print_arenas(name, "the_bridge")
    end)
  
    -- enter editor mode
    cmd:sub("edit :arena", function(sender, arena)
        arena_lib.enter_editor(sender, "the_bridge", arena)
    end)
  
    -- enable and disable arenas
    cmd:sub("enable :arena", function(name, arena)
        arena_lib.enable_arena(name, "the_bridge", arena)
    end)
  
    cmd:sub("disable :arena", function(name, arena)
        arena_lib.disable_arena(name, "the_bridge", arena)
    end)
  
  end, {
    description = [[
  
      (/help the_bridge)
  
      Use this to configure your arena:
      - create <arena name> [min players] [max players]
      - edit <arena name>
      - enable <arena name>
  
      Other commands:
      - remove <arena name>
      - disable <arena>
      ]],
      privs = {
          the_bridge_admin = true
      },
  })
  
  
  arena_lib.on_enable("the_bridge", function(arena, p_name)
    if #arena.the_bridge_area_pos_1 ~= #arena.the_bridge_area_pos_2 then
      minetest.chat_send_player(p_name,"Missing params in the positions")
      return false
    end
    return true
  end)
  
  
