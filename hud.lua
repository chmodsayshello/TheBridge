function create_huds(arena)

    local function create(player)
        scoreboard_blue = minetest.get_player_by_name(player):hud_add({
            hud_elem_type = "text",
            position = {x =0.35, y = 0},
            offset = {x=0, y = 50},
            scale = {x = 100, y = 100},
            text = "0",
            number = 0x0001FF,
        })

        scoreboard_red = minetest.get_player_by_name(player):hud_add({
            hud_elem_type = "text",
            position = {x = 0.65, y = 0},
            offset = {x=0, y = 50},
            scale = {x = 400, y = 1},
            text = "0",
            number = 0xFF0000
        })

        temp = {red=scoreboard_red,blue=scoreboard_blue}
        arena.huds[player] =temp
    end

    for playername in pairs(arena.players) do
        create(playername)
    end
end

function update_huds(arena, player)
    local playername = player:get_player_name()
    player:hud_change(arena.huds[playername].blue, "text", tostring(arena.teams[arena.team_id_blue].goals))
    player:hud_change(arena.huds[playername].red, "text", tostring(arena.teams[arena.team_id_red].goals))
end

function remove_all_huds(arena)
    for playername in pairs(arena.players) do
        local current_player = minetest.get_player_by_name(playername)
        current_player:hud_remove(arena.huds[playername].red)
        current_player:hud_remove(arena.huds[playername].blue)
    end
end