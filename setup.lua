local S = minetest.get_translator("the_bridge")

local function send_message(arena,num_str)
    arena_lib.HUD_send_msg_all("title", arena, num_str, 1,nil,0xFF0000)
    --arena_lib.HUD_send_msg_all(HUD_type, arena, msg, <duration>, <sound>, <color>)
end


function savemap(pos_min,pos_max,arena)
    local manip = VoxelManip(pos_min,pos_max)
    local emin, emax = manip:read_from_map(pos_min, pos_max)
    local data = manip:get_data()
    arena.mapdata = data
    local a = VoxelArea:new{
        MinEdge = emin,
        MaxEdge = emax
    }
    for x = emin.x,emax.x do
        for y = emin.y,emax.y do
            for z = emin.z, emax.z do
                local idx = a:index(x, y, z)
                arena.mapdata[idx]=data[idx]
            end
        end
    end
end

function restore_map(arena)
    local manip = VoxelManip(arena.the_bridge_area_pos_1,arena.the_bridge_area_pos_2)
    manip:set_data(arena.mapdata)
    manip:write_to_map(true)
end


arena_lib.on_load("the_bridge", function(arena)
    savemap(arena.the_bridge_area_pos_1,arena.the_bridge_area_pos_2,arena)--backup map
    if not arena.teams_enabled then
        send_message(arena,"cancelled!")
        arena_lib.force_arena_ending("the_bridge", arena)
        minetest.log("warning","The Bridge is not supposed to run without any teams!")
    end --this game is not supposed to be used without teams!
    arena.team_id_red = get_team_id_by_name(arena_lib.mods["the_bridge"],S("red"))
    arena.team_id_blue = get_team_id_by_name(arena_lib.mods["the_bridge"],S("blue"))
    create_huds(arena)
    after_goal(arena) --gives all players their items and teleports them to spawners

    send_message(arena,S("GO!"))
    arena.setup = true
end)

arena_lib.on_enable("the_bridge", function(arena, p_name)
    savemap(arena.the_bridge_area_pos_1,arena.the_bridge_area_pos_2,arena)
    return true
end)

arena_lib.on_end("the_bridge", function(arena, players, winners, spectators, is_forced)
    restore_map(arena)
    arena.huds = {}
    arena.finished = false
end)

arena_lib.on_disable("the_bridge", function(arena, p_name)
    savemap(arena.the_bridge_area_pos_1,arena.the_bridge_area_pos_2,arena)
    return true
end)

arena_lib.on_quit("the_bridge", function(arena, p_name, is_spectator, reason)
    if is_spectator then return end
    player = minetest.get_player_by_name(p_name)
    player:hud_remove(arena.huds[p_name].blue)
    player:hud_remove(arena.huds[p_name].red)
end)
