local S = minetest.get_translator("the_bridge")

local function set_player_inventory(player)
    local stonestack = ItemStack("the_bridge:stone")
    local pickstack = ItemStack("the_bridge:diapick")
    local swordstack = ItemStack("the_bridge:stonesword")
    local gapplestack = ItemStack("the_bridge:gapple 16")
    local stickstack = ItemStack("the_bridge:knockback_stick")

    local inv = player:get_inventory()
    local hotbar = {swordstack,pickstack,gapplestack,stickstack,stonestack}
    if minetest.get_modpath("grapple") then
        table.insert(hotbar,ItemStack("grapple:grapple"))
    end
    inv:set_list("main", hotbar)
end

function after_goal(arena) --global because its called once outside of this file
    restore_map(arena)
    local players_team_red = arena_lib.get_players_in_team(arena, arena.team_id_red, true)
    local players_team_blue = arena_lib.get_players_in_team(arena, arena.team_id_blue, true)

    for player_index in ipairs(players_team_red) do
        local player = players_team_red[player_index]
        set_player_inventory(player)
        player:set_hp(20)
        player:set_pos(arena_lib.get_random_spawner(arena, arena.team_id_red))
        update_huds(arena,player)
        minetest.sound_play("ding", {
            to_player = player:get_player_name(),
            gain = 2.0,
        })
    end

    for player_index in ipairs(players_team_blue) do
        local player = players_team_blue[player_index]
        set_player_inventory(player)
        player:set_hp(20)
        player:set_pos(arena_lib.get_random_spawner(arena, arena.team_id_blue))
        update_huds(arena,player)
        minetest.sound_play("ding", {
            to_player = player:get_player_name(),
            gain = 2.0,
        })
    end
    if not arena.setup then return end
    time = os.clock()
    while (os.clock() < time +3) do end
    arena.scoring = false
end

local goalfunc = function(pos, node, player)
    if not player then return end
    arena = arena_lib.get_arena_by_player(player:get_player_name())
    if not arena then return end
    
    nodename = node.name
    if nodename == "the_bridge:goal_area_red" then
        if arena.scoring then return end
        local temp = arena.teams[arena.team_id_red].goals +1
        arena.teams[arena.team_id_red].goals = temp
        if temp > 4 and arena.in_game then
            if arena.finished then return end
            arena_lib.load_celebration("the_bridge", arena, arena.team_id_red)
            arena.finished = true
            return
        end
        after_goal(arena)

    elseif nodename == "the_bridge:goal_area_blue" then
        if arena.scoring then return end
        local temp = arena.teams[arena.team_id_blue].goals +1
        arena.teams[arena.team_id_blue].goals = temp
        if temp > 4 and  arena.in_game then
            if arena.finished then return end
            arena_lib.load_celebration("the_bridge", arena, arena.team_id_blue)
            arena.finished = true
            return
        end
        after_goal(arena)
    end
end

arena_lib.on_death("the_bridge", function(arena, p_name, reason)
    local players_team_red = arena_lib.get_players_in_team(arena, arena.team_id_red, false)
    local players_team_blue = arena_lib.get_players_in_team(arena, arena.team_id_blue, false)

    for player_index in ipairs(players_team_red) do
        if players_team_red[player_index] == p_name then
            minetest.get_player_by_name(p_name):set_pos(arena_lib.get_random_spawner(arena, arena.team_id_red))
        end
    end

    for player_index in ipairs(players_team_blue) do
        if players_team_blue[player_index] == p_name then
            minetest.get_player_by_name(p_name):set_pos(arena_lib.get_random_spawner(arena, arena.team_id_blue))
        end
    end

    set_player_inventory(minetest.get_player_by_name(p_name))
    minetest.get_player_by_name(p_name):set_hp(20)
end)

arena_lib.on_celebration("the_bridge", function(arena, winners)
    remove_all_huds(arena)
end)





minetest.register_node("the_bridge:goal_area_red",{
    description = S("Red Goal"),
    drawtype = "glasslike",
    tiles = {"^[colorize:#d9405e"},
    paramtype = "light",
    sunlight_propagates = true,

    walkable     = true,
    pointable    = false,
    diggable     = false,
    buildable_to = false,
    drop = "",
    on_walk_over=goalfunc,
})

minetest.register_node("the_bridge:stone", {
	description = S("Stone"),
	tiles = {"default_stone.png"},
	groups = {cracky = 3, stone = 1},
	drop = "the_bridge:stone",
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
    on_place = function(itemstack, placer, pointed_thing)
        if pointed_thing.type == "node" then
                local pos = pointed_thing.above
                minetest.set_node(pos , {name="the_bridge:stone"})
        end
    end,
    on_dig = function(pos, node, digger)
        minetest.remove_node(pos)
    end,

})

minetest.register_node("the_bridge:goal_area_blue",{
    description = S("Blue Goal"),
    drawtype = "glasslike",
    tiles = {"^[colorize:#3548da"},
    paramtype = "light",
    sunlight_propagates = true,

    walkable     = true,
    pointable    = true,
    diggable     = false,
    buildable_to = false,
    drop = "",
    on_walk_over=goalfunc,
})

minetest.register_node("the_bridge:void", {
    description = S("The Bridge void"),
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,
    walkable     = true,
    pointable    = true,
    diggable     = false,
    buildable_to = false,
    drop = "",
    on_walk_over=function(pos, node, player)
        player:set_hp(0)
    end,
})

minetest.register_tool("the_bridge:stonesword", {
	description = S("Stone Sword"),
	inventory_image = "default_tool_stonesword.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=0,
		damage_groups = {fleshy=8},
	},
})

minetest.register_tool("the_bridge:diapick", {
	description = S("Diamond Pickaxe"),
	inventory_image = "default_tool_diamondpick.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=1.0}, uses=0, maxlevel=3},
		},
	},
})


local gappleeat = minetest.item_eat(6,"")
minetest.register_craftitem("the_bridge:gapple",{
    on_place = gappleeat,
    on_secondary_use = gappleeat,
    on_use=gappleeat,
    description = S("Golden Apple"),
	inventory_image = "default_apple.png^[colorize:#ead535",
})

minetest.register_craftitem("the_bridge:knockback_stick",{
    inventory_image = "default_stick.png",
    on_use = function(itemstack, user, pointed_thing) --parts of the code inside of this function are from https://gitlab.com/zughy-friends-minetest/sumo/-/blob/master/items.lua
        if not pointed_thing then return end
        if not minetest.is_player(pointed_thing.ref) then return end
        dir = user:get_look_dir()
        vel_vector = vector.multiply(dir,12)
        vel_vector.y = 0.5 --hardcoded so you cant launch a player up like 20 blocks
        pointed_thing.ref:add_player_velocity(vel_vector)
    end,
})
