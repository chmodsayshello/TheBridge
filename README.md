# TheBridge

A minetest minigame where your team has to be the first one to cross the bridge, enter the enemy's base and jump into a specific hole 5 times in order to win

Version: 1.0
--

# Setup
Install dependencies
--
- arenalib (https://gitlab.com/zughy-friends-minetest/arena_lib)
- walkover (https://github.com/eidy/walkover)

Setup an arena
--
1.	run `/the_bridge create <name> <min_number_per_team> <max_number_per_team>`
2.	build your arena (make sure to place red and blue goals!)
3.	run `/the_bridge edit <name>`
4.	edit the arena properties, set both positions (corners of your map)
5.	place the spawners
6.	place the sign
